const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const src = path.join(__dirname, 'components');
const distPath = '../build';

const config = {

    entry: {
        vendor: path.join(src, 'vendor.js'),
        app: path.join(src, 'app.js')
    },
    output: {
        path: path.resolve(__dirname, distPath),
        filename: 'js/[name].js'
    },
    resolve: {
        alias: {
            'jquery': path.join(__dirname, 'node_modules', 'jquery'),
            // allows paths to images in pug templates like "~img/path/to.ong"
            'img': path.resolve(__dirname, 'images')
        }
    },
    module: {
        rules: [{
            test: /jquery\.flot\.resize\.js$/,
            use: 'imports-loader?this=>window'
        }, {
            test: /\.pug$/,
            // compile and extract html from pug files
            use: ['file-loader?name=[name].html', 'extract-loader', 'html-loader?attrs[]=img:src&attrs[]=:data-svg-replace&attrs[]=:data-gallery-src', 'pug-html-loader?pretty&exports=false']
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: ['css-loader'],
                publicPath: '../'
            })
        }, {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: ['css-loader', 'sass-loader'],
                publicPath: '../'
            })
        }, {
            test: /\.(ttf|woff|eot|woff2)(\?[\s\S]+)?$/,
            use: 'url-loader?limit=1000&name=fonts/[name].[ext]'
        }, {
            test: /\.(png|jpg|gif|svg)$/,
            use: 'file-loader?name=images/[name].[ext]'
        }]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'js/vendor.js' }),
        // generate stylesheet for each entry
        new ExtractTextPlugin({
            filename: path.join(distPath, 'css/[name].css'),
            allChunks: true
        }),
        // copy static assets used in demo
        new CopyWebpackPlugin([{
            from: 'server',
            to: 'server',
            context: path.join(__dirname, '')
        }]),
        // define globals used in the template
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'window.$': 'jquery',
            'window.moment': 'moment',
            'moment': 'moment',
            'Popper': ['popper.js', 'default'],
            'materialColors': 'material-colors',
            'noUiSlider': 'nouislider',
            'Datamap': 'datamaps',
            'Rickshaw': 'rickshaw',
            'Masonry': 'masonry-layout',
            'screenfull': 'screenfull',
            'moment': 'moment',
            'GMaps': 'gmaps',
            'swal': 'sweetalert'
        })
    ]
};

module.exports = config;