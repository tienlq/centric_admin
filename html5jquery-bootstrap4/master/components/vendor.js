// Modernizr
import './modernizr.js';
// jQuery
import 'jquery';
// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
// jQuery Browser
import 'jquery.browser';
// Animate.CSS
import 'animate.css/animate.css';
// Ionicons
import 'ionicons/css/ionicons.css';
// Screenfull
import 'screenfull';
// Material Colors
import 'material-colors/dist/colors.css';
import 'material-colors';

// Momentjs
import 'moment';
// Loaders CSS
import 'loaders.css/loaders.css.js';
// jQuery Form Validation
import 'jquery-validation';
import 'jquery-validation/dist/additional-methods.js';
// Range Slider
import 'nouislider/distribute/nouislider.min.css';
import 'nouislider';
// ColorPicker
import 'bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css';
// Material Floating Button
import 'ng-material-floating-button/mfb/dist/mfb.css';
// Bootstrap Filestyle
import 'bootstrap-filestyle/src/bootstrap-filestyle.js';
// Flot charts
import 'flot/jquery.flot.js';
import 'flot/jquery.flot.categories.js';
import 'jquery.flot.spline/jquery.flot.spline.js';
import 'jquery.flot.tooltip/js/jquery.flot.tooltip.js';
import 'flot/jquery.flot.resize.js';
import 'flot/jquery.flot.pie.js';
import 'flot/jquery.flot.time.js';
import 'sidebysideimproved/jquery.flot.orderBars.js';
// Rickshaw
import 'rickshaw/rickshaw.css';
import 'rickshaw';
// jVector Maps
import 'ika.jvectormap/jquery-jvectormap-1.2.2.min.js';
import 'ika.jvectormap/jquery-jvectormap-us-mill-en.js';
import 'ika.jvectormap/jquery-jvectormap-world-mill-en.js';
// Easypie Charts
import 'easy-pie-chart/dist/jquery.easypiechart.js';
// Datamaps
import 'datamaps';
// Images Loaded
require('imagesloaded').makeJQueryPlugin($);
// Masonry
import Masonry from 'masonry-layout';
require('jquery-bridget')('masonry', Masonry, $);
// Sparkline
import 'jquery-sparkline/jquery.sparkline.min.js';
// Datepicker
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.css';
import 'bootstrap-datepicker/js/bootstrap-datepicker.js';
// jQuery Knob charts
import 'jquery-knob/js/jquery.knob.js';
// jQuery Steps
import 'jquery-steps/build/jquery.steps.js';
// Select2
import 'select2/dist/css/select2.css';
import 'select2/dist/js/select2.js';
// Clockpicker
import 'clockpicker/dist/bootstrap-clockpicker.css';
import 'clockpicker/dist/bootstrap-clockpicker.js';
// ColorPicker
import 'bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js';
// Summernote
import 'summernote/dist/summernote.css';
import 'summernote/dist/summernote.js';
// Dropzone
import 'dropzone/dist/basic.css';
import 'dropzone/dist/dropzone.css';
import 'dropzone/dist/dropzone.js';
// Xeditable
import 'x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css';
import 'x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js';
// Google Maps
import 'gmaps';
// Bootgrid
import 'jquery-bootgrid/dist/jquery.bootgrid.css';
import 'jquery-bootgrid/dist/jquery.bootgrid.js';
import 'jquery-bootgrid/dist/jquery.bootgrid.fa.js';
// Datatables
require('datatables.net-bs4');
require('datatables.net-buttons');
require('datatables.net-buttons-bs4');
require('datatables.net-responsive');
require('datatables.net-responsive-bs4');
require( 'datatables.net-buttons/js/buttons.colVis.js' ); // Column visibility
require( 'datatables.net-buttons/js/buttons.html5.js' );  // HTML 5 file export
require( 'datatables.net-buttons/js/buttons.flash.js' );  // Flash file export
require( 'datatables.net-buttons/js/buttons.print.js' );  // Print view button
// Nestable
import 'nestable/jquery.nestable.js';
// Sweet Alert
import 'sweetalert/dist/sweetalert.css';
import 'sweetalert';
// jQuery Localize
import 'jquery-localize/dist/jquery.localize.js';
// Bluimp Gallery
import 'blueimp-gallery/css/blueimp-gallery.css';
import 'blueimp-gallery/css/blueimp-gallery-indicator.css';
import 'blueimp-gallery/css/blueimp-gallery-video.css';
import 'blueimp-gallery/js/blueimp-helper.js';
import 'blueimp-gallery/js/blueimp-gallery.js';
import 'blueimp-gallery/js/blueimp-gallery-fullscreen.js';
import 'blueimp-gallery/js/blueimp-gallery-indicator.js';
import 'blueimp-gallery/js/blueimp-gallery-video.js';
import 'blueimp-gallery/js/blueimp-gallery-vimeo.js';
import 'blueimp-gallery/js/blueimp-gallery-youtube.js';
import 'blueimp-gallery/js/jquery.blueimp-gallery.js';
