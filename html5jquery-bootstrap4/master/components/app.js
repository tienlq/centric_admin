/*!
 *
 * Centric - Bootstrap Admin Template
 *
 * Version: 1.9.5
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */

// IMPORTS
// -----------------------------------

import './app.scss';

import './charts/flot.js';
import './charts/radial.js';
import './charts/rickshaw.js';
import './colors/colors.constant.js';
import './colors/colors.js';
import './dashboard/dashboard.js';
import './elements/bootstrapui.js';
import './elements/grid-masonry.js';
import './elements/nestable.js';
import './elements/sweetalert.js';
import './forms/dropzone.js';
import './forms/editor.js';
import './forms/forms.advanced.js';
import './forms/validation.js';
import './forms/wizard.js';
import './forms/xeditable.js';
import './header/header.js';
import './maps/datamaps.js';
import './maps/google-map-full.js';
import './maps/google-map.js';
import './maps/vector-map.js';
import './pages/messages.js';
import './pages/profile.js';
import './ripple/ripple.init.js';
import './ripple/ripple.js';
import './settings/settings.js';
import './sidebar/sidebar.js';
import './tables/bootgrid.js';
import './tables/datatable.js';
import './translate/translates.js';
import './user/lock.js';
import './user/login.js';
import './user/recover.js';
import './user/signup.js';
import './utils/screenfull.js';
import './utils/svgreplace.js';

import './dashboard/dashboard.pug';
import './cards/cards.pug';
import './charts/flot.pug';
import './charts/radial.pug';
import './charts/rickshaw.pug';
import './dashboard/dashboard.pug';
import './elements/bootstrapui.pug';
import './elements/buttons.pug';
import './elements/colors.pug';
import './elements/grid-masonry.pug';
import './elements/grid.pug';
import './elements/icons.pug';
import './elements/lists.pug';
import './elements/nestable.pug';
import './elements/spinners.pug';
import './elements/sweetalert.pug';
import './elements/typography.pug';
import './elements/utilities.pug';
import './elements/whiteframes.pug';
import './forms/dropzone.pug';
import './forms/editor.pug';
import './forms/forms.advanced.pug';
import './forms/forms.classic.pug';
import './forms/material.pug';
import './forms/validation.pug';
import './forms/wizard.pug';
import './forms/xeditable.pug';
import './layouts/layouts.boxed.pug';
import './layouts/layouts.columns.pug';
import './layouts/layouts.containers.pug';
import './layouts/layouts.overlap.pug';
import './layouts/layouts.tabs.pug';
import './maps/datamaps.pug';
import './maps/google-map-full.pug';
import './maps/google-map.pug';
import './maps/vector-map.pug';
import './pages/blog.article.pug';
import './pages/blog.pug';
import './pages/contacts.pug';
import './pages/faq.pug';
import './pages/gallery.pug';
import './pages/invoice.pug';
import './pages/messages.pug';
import './pages/pricing.pug';
import './pages/profile.pug';
import './pages/projects.pug';
import './pages/search.pug';
import './pages/timeline.pug';
import './pages/wall.pug';
import './tables/bootgrid.pug';
import './tables/datatable.pug';
import './tables/tables.classic.pug';
import './user/lock.pug';
import './user/login.pug';
import './user/recover.pug';
import './user/signup.pug';

// APP START
// -----------------------------------

(function() {
    'use strict';

    // Disable warning "Synchronous XMLHttpRequest on the main thread is deprecated.."
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
        options.async = true;
    });

    // Support for float labels on inputs
    $(document).on('change', '.mda-form-control > input', function() {
        $(this)[this.value.length ? 'addClass' : 'removeClass']('has-value');
    });

})();
