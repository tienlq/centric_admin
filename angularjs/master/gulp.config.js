var path = require('path');
var saveLicense = require('uglify-save-license');

module.exports = config();

///////

function config() {

    // MAIN PATHS
    var paths = {
        app: '../app/',
        components: 'components/',
        markup: 'pug/',
        styles: 'less/',
        scripts: 'js/'
    }

    var config = {

        paths: paths,

        // SOURCES CONFIG
        source: {
            scripts: [
                paths.components + 'app.module.js',
                paths.components + '**/*.module.js',
                paths.components + '**/*.js'
            ],
            templates: {
                index: [paths.components + 'index.*'],
                views: [
                    paths.components + '**/*.pug',
                    paths.components + '**/*.html',
                    '!' + paths.components + 'index.*' // ignore index
                ]
            },
            styles: {
                app: [
                    paths.components + 'app.less',
                    paths.components + '**/*.less'
                ],
                watch: [paths.components + '**/*']
            },
            images: [
                'images/**/*'
            ]
        },

        // BUILD TARGET CONFIG
        build: {
            scripts: paths.app + 'js',
            styles: paths.app + 'css',
            templates: {
                index: '../',
                views: paths.app + 'views/',
                cache: paths.app + 'js/' + 'templates.js',
            },
            images: paths.app + 'img',
        },

        // PLUGINS OPTIONS

        prettifyOpts: {
            indent_char: ' ',
            indent_size: 3,
            unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u', 'pre', 'code']
        },

        pugOpts: {
            pretty: true
        },

        uglify: {
            output: {
                comments: saveLicense
            },
            mangle: {
                reserved: ['$super'] // rickshaw requires this
            }
        },

        cleanCss: {
            rebase: false
        },

        tplCacheOptions: {
            // root: 'app',
            filename: 'templates.js',
            //standalone: true,
            module: 'app.core',
            base: function(file) {
                return path.join('app', 'views', path.basename(file.path));
            }
        },

        injectOptions: {
            name: 'templates',
            transform: function(filepath) {
                return 'script(src=\'' +
                    filepath.substr(filepath.indexOf('app')) +
                    '\')';
            }
        }

    }

    return config;
} // config()